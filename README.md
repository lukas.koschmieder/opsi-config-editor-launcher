# OPSI Config Editor Launcher

Launch IEHK OPSI Config Editor in Bash

OPSI Config Editor Launcher will attempt to launch OPSI Config Editor via Java Web Start. The program will either use the OPSI server URL provided as $1 (if available) or it fall back to a default URL defined in [opsi.sh](opsi.sh).

## Usage

    opsi.sh [OPSI_URL]

  * **OPSI_URL** (Optional) OPSI server URL
