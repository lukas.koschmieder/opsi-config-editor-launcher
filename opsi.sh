#!/bin/bash
# OPSI Config Editor Launcher for IEHK
# (c) 2015-2017 Lukas Koschmieder <lukas.koschmieder@rwth-aachen.de>

# Default to given URL if $1 is not specified
SERVER=${1:-"https://opsiserver4:4447"}

# Try to load Java module if Java Web Start is not available
if ! hash javaws 2>/dev/null
then module load java
fi

# Launch OPSI Config Editor
javaws $SERVER/configed.jnlp
